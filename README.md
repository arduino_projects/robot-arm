# Roboter Greifarm 

## Name
Roboter Greifarm mit Arduino

## Description
Ein Roboter Greifarm, welcher mit Drehpotentiometern für jeden Servo gesteuert wird.

## Installation
Vorhandenen Code auf den Arduino laden, den Bildern entsprechend verkabeln (Hilfen als Kommentar auch im Code). 

## Usage
Feel free to be creative.

## Authors and acknowledgment
AirFlow

## License
This is not an open-source project. By this in default - all rights are reserved -. Meaning that no one is allowed to make any copy/ sell/ modify/etc. of this Code. 


## Project status
Nearly finished, maybe an UI will follow.
