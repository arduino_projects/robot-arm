#include <Servo.h>

Servo myservo; // Servo: rot (mitte) an 5V, gelb (links) an analogout ("~"), braun (rechts) an Ground
Servo servo2;
Servo servo3;
Servo servo4;

int e1; //eingabe1, etc.
int e2; //Drehpotentiometer: erstes "Bein" an 5V, mittleres an den Analogen Input, letztes (rechts) an Ground
int e3;
int e4;

int pos=0;
int count=1;


void setup() 
{
   //Serial.begin(9600); kommt nicht mit Servos klar ka why

  myservo.attach(9); 
  servo2.attach(11);
  servo3.attach(10);
  servo4.attach(6);

  e1=0;
  e2=0;
  e3=0;
  e4=0;
  
  
}

void loop() 
{
    e1 = analogRead(0);
    e2 = analogRead(1);
    e3 = analogRead(2);
    e4 = analogRead(3);

    
    myservo.write(eingabeZuGrad(e1));
    servo2.write(eingabeZuGrad(e2));
    servo3.write(eingabeZuGrad(e3));
    servo4.write(eingabeZuGrad(e4));
    /*
    myservo.write(pos);
    pos=pos+count;
    if(pos == 180)
    {
      count=-count; nur zum testen
    }
    */
    
    //Serial.println(e1);
    //Serial.println(map(e1,0,1023,0,360));
    //Serial.println(pos);
    
    //myservo.write(e1);
}

int eingabeZuGrad(int eingabe)
{
  int grad = map(eingabe,0,1023,0,360);    // map(eingabe, altesLow, altesHigh, neuesLow, neuesHigh)
  return grad;
}

// verkürzter Sketch bei dem wir uns zumindestens sicher sind das, falls estwas schief geht, es an der Hardware liegt
